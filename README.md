#qcm
---
**Version 1.0.0**

Tester vos connaissance générale sur ces questions de géographie, histoire,...

- single page application
- chargement de données à partir d'un fichier json
- compte à rebours pour chaque question 
- interactivité (animation jquery pour les transitions, couleur dynamique en fonction de la bonne réponse)
- barre de progression pour indiquer la progression dans le qcm
- feedback final sur le score
- recommencer le jeu

Technologies:

- HTML, CSS, Boostrap
- JQuery


---

## License & copyright
© Sarobidy RAPETERAMANA
