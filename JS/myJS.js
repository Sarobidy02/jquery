var categorie = $(".categorie");
var question = $("h3");
var button = $(".bou");
var points = $("#bonne_reponse h4");
var progress = $(".progress-bar");
var time = $(".rouge");
var qcm = $("#qcm");
var emoji = $("#place_emo");
var score_final = $("#score");
var message = $("#message");
var recommencer = $(".recommencer");
console.log(recommencer);
var liste_question;
var etat_jeu;
var active = true;//the buttons state
var compteur_temps = 10;
$("#fin").hide();//cacher le div du scor
function compteAReb(){
    return setInterval(()=>{
        if(compteur_temps == 0){
            nextQuestion();
        }
        compteur_temps--; 
        time.text(compteur_temps);
    }, 1000);
}
var deconte = compteAReb();

function updateProgress(){
    var numero_question = etat_jeu.question+1;
    var nb_question = liste_question.length;
    var pourcentage = (numero_question * 100)/nb_question;
    progress.css("width", Math.trunc(pourcentage)+"%");
}
function updateButton(){
    var i = 0;
    for(element of button){
        element.textContent = liste_question[etat_jeu.question].proposition[i];
        i++;
    }
}
function updateDom(){
    categorie.text(liste_question[etat_jeu.question].categorie);
    question.text(liste_question[etat_jeu.question].question);
    updateButton();
    time.text(compteur_temps);
    points.text("Points: "+etat_jeu.nombre_points);
    updateProgress();
}
function initialise_doc(data){
    liste_question = data;
    etat_jeu = {
        nombre_points : 0,
        question : 0,
    };
    updateDom();
};
function remove_color(){
    for(element of button){
        element.classList.remove("faux");
        element.classList.remove("vrai");
    }
}
function reveal_false(id_correct){
    var i = 0;
    for(element of button){
        if(i!=id_correct){
            element.classList.add("faux");
        }
        i++;
    }
}
function reveal_true(id){
    button[liste_question[etat_jeu.question].reponse].classList.add("vrai");
    if(liste_question[etat_jeu.question].reponse != id){//mauvaise réponse
        reveal_false(liste_question[etat_jeu.question].reponse);
        return false;
    }
    return true;
}
function nextQuestion(){
    etat_jeu.question++;
    if(etat_jeu.question<liste_question.length){
        remove_color();
        active = true;
        updateDom();
        compteur_temps = 10;
    }
    else{
        clearTimeout(deconte);
        remove_color();
        qcm.fadeOut();
        var teste = (etat_jeu.nombre_points<Math.trunc(liste_question.length/2));
        var emo = teste?"<svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-emoji-expressionless\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\""+">"+
                "<path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\""+"/>"+
                "<path fill-rule=\"evenodd\" d=\"M4 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5zm5 0a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z\""+"/>"+
            "</svg>" : "<svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-emoji-sunglasses\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\""+">"+
            "<path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\""+"/>"+
            "<path fill-rule=\"evenodd\" d=\"M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM6.5 6.497V6.5h-1c0-.568.447-.947.862-1.154C6.807 5.123 7.387 5 8 5s1.193.123 1.638.346c.415.207.862.586.862 1.154h-1v-.003l-.003-.01a.213.213 0 0 0-.036-.053.86.86 0 0 0-.27-.194C8.91 6.1 8.49 6 8 6c-.491 0-.912.1-1.19.24a.86.86 0 0 0-.271.194.213.213 0 0 0-.036.054l-.003.01z\""+"/>"+
            "<path d=\"M2.31 5.243A1 1 0 0 1 3.28 4H6a1 1 0 0 1 1 1v1a2 2 0 0 1-2 2h-.438a2 2 0 0 1-1.94-1.515L2.31 5.243zM9 5a1 1 0 0 1 1-1h2.72a1 1 0 0 1 .97 1.243l-.311 1.242A2 2 0 0 1 11.439 8H11a2 2 0 0 1-2-2V5z\""+"/>"+
          "</svg>";
        var score = etat_jeu.nombre_points+"/"+liste_question.length;
        var mess = teste?"C'est pas ouffffff":"C'est pas malllll";
        emoji.html(emo);
        score_final.text("Score: "+score);
        message.text(mess);
        window.setTimeout(()=>{$("#fin").fadeIn();},700);
    }
}
button.click((e)=>{
    if(active){
        if(reveal_true(e.target.attributes.reponse.value)){
            etat_jeu.nombre_points++;
        }
        active = false;
        setTimeout(nextQuestion, 2000);
    }
});
recommencer.click(()=>{
    console.log("click");
    $("#fin").fadeOut();
    etat_jeu.nombre_points = 0;
    etat_jeu.question = 0;
    active = true;
    compteur_temps = 10;
    setTimeout(()=>{
        updateDom();
        qcm.fadeIn();
        deconte = compteAReb();
    },1000);
});
$(document).ready(()=>{
    $.getJSON( "./resource/data.json", function (data){
        initialise_doc(data);
    });
    
});